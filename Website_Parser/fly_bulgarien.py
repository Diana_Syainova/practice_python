"""
Flybulgarien flight search.

This script checks input arguments, extracts information about flights
and prints it.
"""

import re
import sys

from datetime import datetime, timedelta

import requests
from lxml import etree


class WrongIata(Exception):
    """Exception for wrong iata city code."""


class WrongDate(Exception):
    """Exception for wrong date value."""


class EmptyQueryResult(Exception):
    """Exception for case where found not flights."""

    def __str__(self):
        """Message of exception."""
        return "No flights found for specified parameters"


class InputError(Exception):
    """Exception for wrong input args."""


def get_params(args):
    """Return parameters for request."""
    params = {'lang': 'en', 'depdate': args[2], 'aptcode1': args[0],
              'aptcode2': args[1], 'paxcount': 1, 'infcount': ''}

    if len(args) == 3:
        params.update({'ow': ''})
        flag_one_way = True
    else:
        params.update({'rtdate': args[3], 'rt': ''})
        flag_one_way = False

    return params, flag_one_way


def get_html_response(url_site, parameters):
    """Send request and returns html response."""
    try:
        html = requests.get(url_site, params=parameters, verify=False,
                            timeout=60)
        html_str = html.text
        html.raise_for_status()
        return etree.HTML(html_str)
    except requests.exceptions.HTTPError as t_err:
        print(t_err)
    except requests.exceptions.Timeout as t_err:
        print(t_err)


def format_input_date(string_date):
    """Return input date in correct format."""
    return datetime.strptime(string_date, "%d.%m.%Y").date()


def format_site_date(string_date):
    """Return site date in correct format."""
    return datetime.strptime(string_date, "%a, %d %b %y").date()


def extract_flights_info(html_document, path):
    """Extract flights information from html-document by path to table."""
    flights_info = []

    for row in html_document.xpath(path):
        col = row.xpath('./td/text()')
        if col:
            flights_info.append(col)

    return flights_info


def parse_flights(html_document, date, flag_one_way):
    """Parse and collects all information about flights."""
    path_to_table = '/html/body/form[@id="form1"]/div' \
                    '/table[@id="flywiz"]/tr/td' \
                    '/table[@id="flywiz_tblQuotes"]'

    if flag_one_way:
        path_to_flights_inf = path_to_table + \
                              '/tr[starts-with(@id, "flywiz_rinf")]'
        path_to_flights_prc = '' + path_to_table + \
                              '/tr[starts-with(@id, "flywiz_rprc")]'
    else:
        path_to_flights_inf = path_to_table + \
                              '/tr[starts-with(@id, "flywiz_irinf")]'
        path_to_flights_prc = '' + path_to_table + \
                              '/tr[starts-with(@id, "flywiz_irprc")]'

    flights_inf = extract_flights_info(html_document, path_to_flights_inf)
    flights_prc = extract_flights_info(html_document, path_to_flights_prc)

    # Site does not contain information about flight classes now.
    # All flights will be Economy.
    flight_class = "Economy"

    flights = []
    for i, _ in enumerate(flights_inf):
        flight = flights_inf[i]
        if date == format_site_date(flight[0]):
            flight.extend(flights_prc[i])
            flight.append(flight_class)
            flights.append(flight)

    if not flights:
        raise EmptyQueryResult

    return flights


def format_time(str_time):
    """Return time in correct format."""
    return datetime.strptime(str_time, "%H:%M")


def flight_duration(departure_time, arrival_time):
    """Compute flight duration."""
    departure = format_time(departure_time)
    arrival = format_time(arrival_time)
    if departure > arrival:
        arrival += timedelta(days=1)

    return arrival - departure


def get_output_format_flights(flights):
    """Return flights for specified date and direction."""
    return [[elem[1], elem[2],
             str(flight_duration(flights[0][1],
                                 flights[0][2])),
             re.search(r"\d+.\d+ \w+[^0-9_]", elem[5]).group(),
             elem[len(elem) - 1]]
            for elem in flights]


def complete_flight_route(flight_out, flight_back):
    """Return complete flight."""
    if re.search("[a-zA-Zа-яА-ЯёЁ]+", flight_out[3]).group() == re.search(
            "[a-zA-Zа-яА-ЯёЁ]+", flight_back[3]).group():
        price_to = float(re.search(r"\d+.\d+", flight_out[3]).group())
        price_back = float(re.search(r"\d+.\d+", flight_back[3]).group())
        flight = [flight_out, flight_back,
                  str(price_to + price_back) + ' ' + re.search(
                      "[a-zA-Zа-яА-ЯёЁ]+", flight_out[3]).group()]
    else:
        flight = [flight_out, flight_back]

    return flight


def check_time_flights_out_back(flight_out, flight_back, flag_border):
    """Check border time."""
    if flag_border == 0:
        if format_time(flight_out[1]) < format_time(flight_out[0]) or (
                format_time(flight_back[0]) < format_time(flight_out[1])):
            return False
    if flag_border == 1:
        if format_time(flight_out[1]) < format_time(flight_out[0]) and (
                format_time(flight_out[1]) > format_time(flight_back[0])):
            return False

    return True


def formation_all_flights_routes(flights_out, flights_back, flag_border):
    """Return all combinations of complete flights."""
    complete_flights = []

    for out in flights_out:
        for back in flights_back:
            if check_time_flights_out_back(out, back,
                                           flag_border):
                complete_flight = complete_flight_route(out, back)
                complete_flights.append(complete_flight)

    return complete_flights


def flights_information(args):
    """Parse arguments and prints flights information."""
    url_fly = 'https://apps.penguin.bg/fly/quote3.aspx'
    input_data, is_one_way = get_params(args)
    html_doc = get_html_response(url_fly, input_data)

    date_to = format_input_date(input_data['depdate'])
    flights_to = get_output_format_flights(parse_flights(html_doc, date_to,
                                                         True))

    if not is_one_way:
        date_back = format_input_date(input_data['rtdate'])
        flights_back = get_output_format_flights(parse_flights(html_doc,
                                                               date_back,
                                                               is_one_way))

        date_diff = (date_back - date_to).days

        flights_to_and_back = formation_all_flights_routes(flights_to,
                                                           flights_back,
                                                           date_diff)
        if not flights_to_and_back:
            raise EmptyQueryResult

        return sorted(flights_to_and_back, key=lambda flight: flight[2])

    return flights_to


def codes_iata(url_site):
    """Return all IATA codes."""
    site = get_html_response(url_site, '')

    path_to_codes = '/html/body/div[@id="wrapper"]/div[@id="content"]' \
                    '/div[@id="reserve"]/form[@id="reserve-form"]/dl/dd' \
                    '/select[@id="departure-city"]/option/@value'

    return [c for c in site.xpath(path_to_codes) if c != '']


def check_date(date_str):
    """Check input date."""
    try:
        date = format_input_date(date_str)
    except ValueError:
        raise WrongDate(
            "Wrong date value: {}. Date example: 21.07.2019".format(date_str))
    if date < datetime.now().date():
        raise WrongDate("Date {} is in the past".format(date_str))


def check_code(code, iata_codes):
    """Check input IATA code."""
    if code not in iata_codes:
        raise WrongIata("Wrong IATA code: {}".format(code))


def check_arguments(args):
    """Check all input arguments."""
    if len(args) < 3 or len(args) > 4:
        raise InputError("Wrong args len. Should be 3 or 4")

    url_isx_site = 'http://www.flybulgarien.dk/en/'
    codes_iata_isx = codes_iata(url_isx_site)
    check_code(args[0], codes_iata_isx)
    check_code(args[1], codes_iata_isx)

    check_date(args[2])
    if len(args) == 4:
        check_date(args[3])
        if format_input_date(args[2]) > format_input_date(args[3]):
            raise WrongDate("Date of coming back can not be earlier than date "
                            "of departure")


def scrape(flights_result, flag_return):
    """Print flight search results."""
    if flag_return == 3:
        for flight in flights_result:
            for flight_information in flight:
                print(flight_information, end=" ")
            print()
    elif flag_return == 4:
        for flight in flights_result:
            for flight_components in flight:
                for flight_information in flight_components:
                    print(flight_information, end=" ")
                print()
            print()


if __name__ == '__main__':
    while True:
        try:
            if 3 <= len(sys.argv) <= 4:
                ARGS = sys.argv[1:]
            else:
                print('')
                print('Please enter the correct arguments separated '
                      'by a space:')
                print('IATA city code from which you want to fly '
                      'in the format of three capital letters for example: '
                      'CPH for Copenhagen')
                print('IATA city city in which you want to fly in the format '
                      'of three capital letters for example: VAR for Varna')
                print('Date of departure in the format: 21.07.2019')
                print('Optional parameter date of coming back in the format: '
                      '30.08.2019')
                ARGS = input().split()

            check_arguments(ARGS)
        except (InputError, WrongIata, WrongDate) as err:
            print(str(err))
            continue
        else:
            break

    try:
        scrape(flights_information(ARGS), len(ARGS))
    except EmptyQueryResult as err:
        print(str(err))
