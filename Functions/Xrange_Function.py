def new_xrange(*args):
    start = 0
    step = 1

    if len(args) == 3:
        start = args[0]
        stop = args[1]
        step = args[2]
    elif len(args) == 2:
        start = args[0]
        stop = args[1]
    elif len(args) == 1:
        stop = args
    else:
        raise ValueError("Please specify from 1 to 3 arguments: (stop) or (start, stop) or (start, stop, step)")

    element = start
    while element < stop:
        yield element
        element += step
