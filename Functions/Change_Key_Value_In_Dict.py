def swap_key_and_value(dictionary):
    try:
        return {v: k for k, v in dictionary.items()}
    except:
        print("It's impossible to make a dictionary")
