def shortest_length(*args):
    list_length = [len(args[i]) for i in range(len(args))]

    return min(list_length)


def new_zip_py2(*args):

    return [tuple([args[i][j] for i in range(len(args))]) for j in range(shortest_length(*args))]


def new_zip_py3(*args):

    return (tuple([args[i][j] for i in range(len(args))]) for j in range(shortest_length(*args)))
