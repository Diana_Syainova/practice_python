def squares_numbers(numbers):
    return [pow(x, 2) for x in numbers]


def numbers_with_even_positions(numbers):
    return [numbers[i] for i in range(1, len(numbers), 2)]


def squares_even_numbers_on_odd_positions(numbers):
    return [pow(numbers[i], 2) for i in range(1, len(numbers), 2) if numbers[i] % 2 == 0]
