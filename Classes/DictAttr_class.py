class DictAttr(object):
    def __init__(self, list_attribute):
        dict_attribute = dict(list_attribute)
        self.__dict__.update(dict_attribute)

    def __str__(self):
        attributes = ['{}: {}'.format(repr(attr), self.__dict__[attr]) for attr in sorted(self.__dict__)]

        return '{ ' + ', '.join(attributes) + '}'

    def __getitem__(self, index):
        return self.__dict__[index]

    def get(self, *args):
        if args and (args[0] in self.__dict__):
            return self.__dict__[args[0]]
        else:
            if len(args) == 1:
                return
            elif len(args) == 2:
                return args[1]
            else:
                raise TypeError


if __name__ == "__main__":
    x = DictAttr([('one', 1), ('two', 2), ('three', 3)])
    print(x)
    print(x['three'])
    print(x.get('one'))
    print(x.get('five'))
    print(x.get('five', 'missing'))
    print(x.one)
    # print(x.five)
