# coding: utf-8

"""
Напишите декоратор memoize, который сохраняет результат вычисления функции
и при последующих вызовах возвращает сохранённое значение, не вычисляя функцию повторно.

Предполагается, что у функции нет входных параметров (аргументов).
"""


import random


def memoize(func):
    state = []

    def get_or_compute():
        if not state:
            state.append(func())
        return state[0]

    return get_or_compute


@memoize
def test_function_decorated():
    return random.randint(1, 100)


def test_function():
    return random.randint(1, 100)


if __name__ == "__main__":
    print(test_function_decorated(), test_function_decorated())
    print(test_function(), test_function())
