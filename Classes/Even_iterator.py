# coding: utf-8

"""
Напишите итератор EvenIterator, который позволяет получить из списка
все элементы, стоящие на чётных индексах.
"""


class EvenIterator(object):
    def __init__(self, list_elements):
        self.elements = list_elements[:]
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= len(self.elements):
            raise StopIteration

        element = self.elements[self.index]
        self.index += 2

        return element


if __name__ == '__main__':
    x = EvenIterator([1, 2, 3, 4, 5, 6, 7, 8])
    for i in x:
        print(i, end=' ')

    y = EvenIterator(['one', 'two', 'three', 'four', 'five'])
    i = iter(y)
    print('')
    print(next(i))
    print(next(i))
    print(next(i))
