# coding: utf-8

"""
Напишите генератор odd_generator, который возвращает все нечётные
числа от 1 до n.
"""


def odd_generator(n):
    for i in range(1, n, 2):
        yield i
