class Observable(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __str__(self):
        public_attributes = ['{}={}'.format(attribute, repr(self.__dict__[attribute]))
                             for attribute in sorted(self.__dict__)
                             if not attribute.startswith("_")]

        str_attributes = str(self.__class__.__name__) + '(' + ', '.join(public_attributes) + ')'

        return str_attributes


class X(Observable):
    pass


if __name__ == '__main__':
    x = X(foo=1, bar=5, _bazz=12, name='Amok', props=('One', 'two'))
    print(x)
    print(x.foo)
    print(x.name)
    print(x._bazz)
