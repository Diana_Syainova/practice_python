class XDictAttr(object):
    def __init__(self, list_attribute):
        dict_attribute = dict(list_attribute)
        self.__dict__.update(dict_attribute)

    def __str__(self):
        attributes = ['{}: {}'.format(repr(attr), self.__dict__[attr]) for attr in sorted(self.__dict__)]
        return str(self.__class__.__name__) + ': { ' + ', '.join(attributes) + '}'

    def extract_from_child_get_key(self, index):
        name_function = 'get_' + index
        if name_function in self.__dir__():
            return getattr(self, name_function)()
        else:
            raise AttributeError

    def __getitem__(self, index):
        if index in self.__dict__:
            return self.__dict__[index]
        else:
            return self.extract_from_child_get_key(index)

    def __getattr__(self, index):
        return self.extract_from_child_get_key(index)

    def get(self, *args):
        if args[0] in self.__dict__:
            return self.__dict__[args[0]]
        else:
            try:
                return self.extract_from_child_get_key(args[0])
            except AttributeError:
                if len(args) == 1:
                    return
                elif len(args) == 2:
                    return args[1]
                else:
                    raise TypeError


class X(XDictAttr):
    def get_foo(self):
        return 5

    def get_bar(self):
        return 12


if __name__ == "__main__":
    x = X({'one': 1, 'two': 2, 'three': 3})
    print(x)
    print(x['one'])
    print(x.three)
    print(x.bar)
    print(x['foo'])
    print(x.get('foo', 'missing'))
    print(x.get('bzz', 'missing'))
