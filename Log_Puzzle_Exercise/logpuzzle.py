#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib
import urllib.request

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.
"""


def extract_file(filename):
    f = open(filename)
    text = f.read()

    return text


def read_urls(filename):
    """Returns a list of the puzzle urls from the given log file,
    extracting the hostname from the filename itself.
    Screens out duplicate urls and returns the urls sorted into
    increasing order.
    """
    file = extract_file(filename)

    base_urls = re.findall(r'\S*puzzle\S*.jpg', file)
    index_server_name = filename.find('_')
    server_name = filename[index_server_name + 1:]
    urls = set()

    for url in base_urls:
        urls.add('http://' + server_name + url)

    return list(urls)


def download_images(img_urls, dest_dir):
    """Given the urls already in the correct order, downloads
    each image into the given directory.
    Gives the images local filenames img0, img1, and so on.
    Creates an index.html in the directory
    with an img tag to show each local image file.
    Creates the directory if necessary.
    """
    abs_dest_dir = os.path.abspath(dest_dir)
    if not os.path.exists(abs_dest_dir):
        os.mkdir(abs_dest_dir)

    image = []

    for i in range(len(img_urls)):
        print('Downloads: ' + str(i + 1) + ' from ' + str(len(img_urls)))
        image.append(abs_dest_dir + '\\' + 'img' + str(i) + '.jpg')
        urllib.request.urlretrieve(img_urls[i], image[i])

    print('Done')

    file = open(abs_dest_dir + '\\' + 'index.html', "w")
    file.write('<verbatim>' + '\n')
    file.write('<html>' + '\n')
    file.write('<body>' + '\n')
    for item in image:
        file.write('<img src=' + '"' + item + '">')
    file.write('\n' + '</body>' + '\n')
    file.write('</html>' + '\n')

    file.close()


def key_for_sorted(url):
    url_new = url

    if re.match(r'\S+-[a-zA-Z]+-[a-zA-Z]+.jpg', url):
        index = url.rfind('-')
        url_new = url[(index + 1):-4]

    return url_new


def main():
    args = sys.argv[1:]

    if not args:
        print('usage: [--to_dir dir] logfile ')
        sys.exit(1)

    to_dir = ''
    if args[0] == '--to_dir':
        to_dir = args[1]
        del args[0:2]

    img_urls = read_urls(args[0])

    sorted_urls = sorted(img_urls, key=lambda u: key_for_sorted(u))

    if to_dir:
        download_images(sorted_urls, to_dir)
    else:
        print('\n'.join(sorted_urls))


if __name__ == '__main__':
    main()
