def happiness_counter(array, set_a, set_b):
    count_happiness = 0

    for i in array:
        if i in set_a:
            count_happiness += 1
        elif i in set_b:
            count_happiness -= 1

    return count_happiness


if __name__ == '__main__':
    nm = input().split()
    n = int(nm[0])
    m = int(nm[1])
    arr_input = input()
    arr = list(map(int, arr_input.split()))
    A_input = input()
    B_input = input()
    A = set(map(int, A_input.split()))
    B = set(map(int, B_input.split()))

    print(happiness_counter(arr, A, B))
