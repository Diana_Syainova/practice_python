import itertools


def probability_select_char_from_line(size, line, index, char):
    combs = list(itertools.combinations(line, index))
    count_of_sets = 0
    for comb in combs:
        if char in comb != -1:
            count_of_sets += 1

    return count_of_sets / len(combs)


if __name__ == '__main__':
    size_list = int(input())
    chars = input().split()
    indices = int(input())

    probability_a = probability_select_char_from_line(size_list, chars, indices, 'a')

    print(round(probability_a, 3))
