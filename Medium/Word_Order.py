if __name__ == '__main__':
    n = int(input())
    unique_words = []
    dict_words = {}
    i = 1

    while i <= n:
        word = input()

        if word not in unique_words:
            unique_words.append(word)
            dict_words[word] = 1
        else:
            dict_words[word] += 1

        i += 1

    print(len(unique_words))
    for word in unique_words:
        print(dict_words[word], end=' ')
