def minion_game(string):
    vowels = "AEIOU"

    stuart_result = 0
    kevin_result = 0

    for i in range(len(string)):
        if string[i] not in vowels:
            stuart_result += (len(string) - i)
        else:
            kevin_result += (len(string) - i)

    if stuart_result > kevin_result:
        print('Stuart', end=' ')
        print(stuart_result)
    elif stuart_result < kevin_result:
        print('Kevin', end=' ')
        print(kevin_result)
    else:
        print('Draw')


if __name__ == '__main__':
    s = input()
    minion_game(s)
