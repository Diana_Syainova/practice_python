#!/bin/python3

import os
from datetime import datetime


def time_parse(t):
    return datetime.strptime(t, "%a %d %b %Y %H:%M:%S %z")


# Complete the time_delta function below.
def time_delta(t1, t2):
    datetime_t1 = time_parse(t1)
    datetime_t2 = time_parse(t2)

    delta_t = datetime_t1 - datetime_t2

    return abs(int(delta_t.total_seconds()))


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        t1 = input()

        t2 = input()

        delta = time_delta(t1, t2)

        fptr.write(str(delta) + '\n')

    fptr.close()
