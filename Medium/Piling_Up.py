def pile_of_cubes(list_cubes, size_list):
    list_pile = []
    list_pile.extend(list_cubes)
    current_cube = max(list_pile)
    result = 'Yes'

    for i in range(size_list - 1):
        if list_pile[len(list_pile) - 1] < list_pile[0] <= current_cube:
            current_cube = list_pile.pop(0)
        elif list_pile[0] <= list_pile[len(list_pile) - 1] <= current_cube:
            current_cube = list_pile.pop(len(list_pile) - 1)
        else:
            result = 'No'
            break

    return result


if __name__ == '__main__':
    t = int(input())

    for t_itr in range(t):
        size = int(input())

        cubes_isx = input().split()
        cubes = [int(x) for x in cubes_isx]

        answer = pile_of_cubes(cubes, size)
        print(answer)
