def merge_the_tools(line, count_char):
    substrings = list(map(''.join, zip(*[iter(line)] * count_char)))

    for substring in substrings:
        print(''.join(sorted(set(substring), key=substring.index)))


if __name__ == '__main__':
    string, k = input(), int(input())
    merge_the_tools(string, k)
