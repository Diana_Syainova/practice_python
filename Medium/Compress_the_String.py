def compress(line):
    list_line = [int(x) for x in line]
    result = []
    number = list_line[0]
    count_number = 0
    for i in list_line:
        if i == number:
            count_number += 1
        else:
            result.append([count_number, number])
            number = i
            count_number = 1
    result.append([count_number, number])

    return result


def print_result(list_res):
    for i in list_res:
        print('(' + str(i[0]) + ', ' + str(i[1]) + ')', end=' ')


if __name__ == '__main__':
    s = input()
    list_set = compress(s)
    print_result(list_set)
