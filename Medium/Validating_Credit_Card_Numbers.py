import re


def validation(card):
    regular_expressions = [(str(reg) + '-?') * 4 for reg in range(10)]

    if re.fullmatch('[4-6]\d{3}(-?\d{4}){3}', card):
        for expression in regular_expressions:
            if re.search(expression, card):
                return 'Invalid'
        return 'Valid'
    else:
        return 'Invalid'


if __name__ == '__main__':
    size = int(input())

    for i in range(size):
        number_card = input()
        result = validation(number_card)
        print(result)
