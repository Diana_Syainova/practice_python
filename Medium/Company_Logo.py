#!/bin/python3


def words_counter(line):
    words_from_str = list(line.lower())
    unique_words = set(words_from_str)

    words = {x: words_from_str.count(x) for x in unique_words}

    return words


def print_dict(dict_str, count):
    count_words = 0
    for k, v in sorted(dict_str.items(), key=lambda d: (-d[1], d[0])):
        if (count_words < count) and (count <= len(dict_str)):
            print(k, v)
            count_words += 1
        else:
            break


if __name__ == '__main__':
    s = input()
    dict_s = words_counter(s)
    print_dict(dict_s, 3)
