#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import shutil
import sys

"""Copy Special exercise
"""


def get_files(directory):
    abs_path = os.path.abspath(directory)
    files = os.listdir(abs_path)
    files_on_pattern = []

    for file in files:
        if re.match('\w*__\w+[^_]__\w*', file):
            files_on_pattern.append(file)

    return files_on_pattern


def get_special_paths(directories):
    abs_paths_files = []

    for directory in directories:
        abs_dir = os.path.abspath(directory)
        files = get_files(abs_dir)
        for file in files:
            abs_paths_files.append(os.path.join(abs_dir, file))

    return abs_paths_files


def copy_to(files, directory):
    abs_directory = os.path.abspath(directory)
    if not os.path.exists(abs_directory):
        os.mkdir(abs_directory)

    for file in files:
        filename = os.path.basename(file)
        new_abs_path = os.path.join(abs_directory, filename)
        shutil.copyfile(file, new_abs_path)


def zip_to(files, zip_path):
    abs_zip_path = os.path.abspath(zip_path)
    if not os.path.exists(abs_zip_path):
        os.mkdir(abs_zip_path)
    abs_path_file_zip = abs_zip_path + '\zipfile' + '.zip'

    zip_command = 'C:\\"Program Files"\\7-Zip\\7z a "{0}" "{1}" > nul'.format(abs_path_file_zip,
                                                                              '\" \"'.join(files))

    print('Zip command is:')
    print(zip_command)

    if os.system(zip_command) == 0:
        print('Zipped successful', abs_path_file_zip)
    else:
        print('Zipping failed')


def print_special_paths(paths):
    for path in paths:
        print(path)


def main():
    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.

    args = sys.argv[1:]
    if not args:
        print("usage: [--to_dir dir][--to_zip zipfile] dir [dir ...]")
        sys.exit(1)

    # to_dir and to_zip are either set from command line
    # or left as the empty string.
    # The args array is left just containing the dirs.
    to_dir = ''
    if args[0] == '--to_dir':
        to_dir = args[1]
        del args[0:2]

    to_zip = ''
    if args[0] == '--to_zip':
        to_zip = args[1]
        del args[0:2]

    if len(args) == 0:
        print("error: must specify one or more dirs")
        sys.exit(1)

    paths = get_special_paths(args)

    if to_dir != '':
        copy_to(paths, to_dir)
    elif to_zip != '':
        zip_to(paths, to_zip)
    else:
        print_special_paths(paths)


if __name__ == "__main__":
    main()
