import math


def maximazer(list_numbers, mod_value):
    sum_squares = 0

    for item in list_numbers:
        sum_squares += math.pow(max(item), 2)

    return math.fmod(sum_squares, mod_value)


if __name__ == '__main__':
    input_line = input().split()

    size = int(input_line[0])
    module = int(input_line[1])
    numbers_lists = []

    for i in range(size):
        numbers_lists.append([int(x) for x in input().split()[1:]])

    s = maximazer(numbers_lists, module)
    print(int(s))
