#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

# import os
import re
import sys

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""


def extract_file(filename):
    f = open(filename)
    text = f.read()

    return text


def extract_year(filename):
    text = extract_file(filename)
    year = re.search(r'Popularity\sin\s(\d\d\d\d)', text)[1]

    return year


def extract_names(filename):
    file = extract_file(filename)
    rows_table_names = re.findall(r'<td>(\d+)</td><td>(\w+)</td><td>(\w+)</td>', file)
    mapping_rows = []

    for row in rows_table_names:
        mapping_rows.append(row[1] + ' ' + row[0])
        mapping_rows.append(row[2] + ' ' + row[0])

    names = sorted(mapping_rows)

    return names


"""
   Given a file name for baby.html, returns a list starting with the year string
   followed by the name-rank strings in alphabetical order.
   ['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
"""


def year_names(filename):
    year = [extract_year(filename)]
    names = extract_names(filename)

    return year + names


def print_result(item_list):
    for item in item_list:
        print(item)


def write_result_to_file(filename, item_list):
    f = open(filename, "w")

    for item in item_list:
        f.write(item + '\n')
    f.close()


def main():
    # This command-line parsing code is provided.
    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.

    args = sys.argv[1:]

    if not args:
        print('usage: [--summary_file] file [file ...]')
        sys.exit(1)

    # Notice the summary flag and remove it from args if it is present.
    summary = False
    if args[0] == '--summary_file':
        summary = True
        del args[0]

    # For each filename, get the names, then either print the text output
    # or write it to a summary file

    # directory = os.path.dirname(os.path.abspath(sys.argv[0]))
    # files = os.listdir(directory)
    # html_files = filter(lambda f: f.endswith('.html'), files)
    html_files = args

    if summary:
        for filename in html_files:
            filename_summary = filename + '.summary'
            write_result_to_file(filename_summary, year_names(filename))
    else:
        for filename in html_files:
            print_result(year_names(filename))


if __name__ == '__main__':
    main()
